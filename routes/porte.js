const pool = require('./../db');
var json2csv = require('json2csv').parse;
var dateFormat = require('dateformat');

//Récupérer horaires en fonction d'un code et d'un horaire
const getSchedule = (request, response) => {
    var codeInstitution = request.params.code;
    var date = request.params.date;
    var day = dateFormat(date, "yyyy-mm-dd");
    console.log(day)
    console.log(codeInstitution)
    pool.query('SELECT S."open" AS "HeureOuverture", S."close" AS "HeureFermeture" FROM porte.schedule S LEFT JOIN porte."institution" I ON I."id" = S."institutionId" WHERE I."id" = $1 AND S."date" = $2', [codeInstitution, day])
        .then(res => {
            var data = json2csv(res.rows);
            response.attachment(codeInstitution + '-' + date + '-horaires.csv');
            response.status(200).send(data);
        })
        .catch(err => response.status(404).json("Pas d'information trouvé"))
}

module.exports = {
    getSchedule
}