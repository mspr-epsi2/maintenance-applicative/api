var dateFormat = require('dateformat');
const pool = require('./../db');
var json2csv = require('json2csv').parse

//Récupérer horaires
const getLightByDay = (request, response) => {
    const date = request.params.date;
    var day = dateFormat(date, "yyyy-mm-dd");
    pool.query('SELECT latitude AS "Latitude", longitude AS "Longitude" ,S.start AS "Allumage" ,S.stop AS "Extinction" from based."Light" L INNER JOIN based."District" D ON L."district" = D."code" INNER JOIN based."Schedule" S ON S.district = D."code" WHERE S."date" = $1', [day])
        .then(res => {
            var data = json2csv(res.rows);
            response.attachment(date + '-horaires.csv');
            response.status(200).send(data);
        })
        .catch(() => response.status(404).json("Le jour renseigné n'a pas d'horaires"))
}

module.exports = {
    getLightByDay
}