const pool = require('./../db');
const xml2js = require('xml2js');
const fs = require('fs')
const { v4: uuidv4 } = require('uuid');

//Filename to date
function fileNameToDate(filename){
    if(filename == undefined){
        return null;
    }
    var dateParts = filename.split("-");
    var dateObject = new Date(dateParts[2], dateParts[1] - 1, +dateParts[0]);
    return dateObject;
}

//Récupérer info 
function shouldParse(file) {
    return new Promise(function (resolve, reject) {
        console.log(file)
        const parser = new xml2js.Parser({ attrkey: "ATTR" });
        let xml_string = fs.readFileSync(file.path, "utf8");
        var listObject = [];
        var dateObject = fileNameToDate(file.originalname.slice(0, -4))
        parser.parseString(xml_string, function (error, result) {
            if (error === null) {
                result.Document.Arrdt.forEach(arrdt => {
                    var Fn = "";
                    var Db = "";
                    arrdt.Plge.forEach(plge => {
                        if (plge.ATTR.Fn != null) {
                            Fn = plge.ATTR.Fn;
                        }
                        else {
                            Db = plge.ATTR.Db;
                        }
                    })
                    var schedule = {
                        id: uuidv4(),
                        date: dateObject,
                        start: Fn,
                        end: Db,
                        district: arrdt.ATTR.Id
                    }
                    listObject.push(schedule)
                });
                resolve(listObject)
            }
            else {
                reject("error");
            }
        });
    })
}

//Récupérer info team
function insertInBase(tab) {
    return new Promise(function (resolve, reject) {
        tab.forEach(element => {
            pool.query('INSERT INTO based."Schedule" (id,date,start,stop,district) VALUES ($1,$2,$3,$4,$5)', [element.id, element.date, element.start, element.end,element.district])
        })
        resolve("Super")
    })
}


//Parse XML
const setXML = (request, response) => {
    return shouldParse(request.file)
        .then(schedules => {
            return insertInBase(schedules)
            .then(res => response.status(200).json("Insérer en base"))
            .catch(err => response.status(401).json(err))
        })
}

module.exports = {
    setXML,
    shouldParse,
    fileNameToDate
}