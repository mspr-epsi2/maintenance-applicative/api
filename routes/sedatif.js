const pool = require('./../db');
var json2csv = require('json2csv').parse

//Récupérer espaces ouverts
const getOpenAreas = (request, response) => {
    return pool.query('SELECT A.name AS "Nom", A.adress as "Adresse" FROM sedatif."Area" A LEFT JOIN sedatif."Tasks" T ON A.id = T.area WHERE T."endDate" IS null OR T."endDate" < now()')
        .then(res => {
            var data = json2csv(res.rows);
            response.attachment('openAreas.csv');
            response.status(200).send(data);
        })
        .catch(() => response.status(404).json("Error"))
}

//Récupérer espaces ouverts
const getCloseAreas = (request, response) => {
    return pool.query('SELECT A.name AS "Nom", A.adress as "Adresse", to_char(T."endDate",\'dd-mm-yyyy\') AS "DateFinTravaux"  FROM sedatif."Area" A LEFT JOIN sedatif."Tasks" T ON A.id = T.area WHERE T."endDate" > now()')
        .then(res => {
            var data = json2csv(res.rows);
            response.attachment('worksAreas.csv');
            response.status(200).send(data);
        })
        .catch(() => response.status(404).json("Error"))
}

module.exports = {
    getOpenAreas,
    getCloseAreas
}