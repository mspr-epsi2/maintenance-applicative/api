const parse = require('../routes/parse');

test('Should return null', () => {
  expect(parse.fileNameToDate()).toBeNull();
});

test('Should return date', () => {
  expect(typeof(parse.fileNameToDate("20-05-2021"))).toBe(typeof(new Date));
});