const express = require('express')
const app = express()
const port = 3000
const bodyParser = require('body-parser')
var multer = require('multer');
const upload = multer({ dest: 'upload/' });


app.use(bodyParser.json())

//Routes
const based = require('./routes/based')
const sedatif = require('./routes/sedatif')
const porte = require('./routes/porte')
const parser = require('./routes/parse')

//Based
app.get('/eclairage/horaires/:date', based.getLightByDay)

//Sedatif
app.get('/espaces/ouverts', sedatif.getOpenAreas)
app.get('/espaces/travaux', sedatif.getCloseAreas)

//Porte
app.get('/horaires/:code/:date', porte.getSchedule)

//Parser
app.post('/parse', upload.single('file'), parser.setXML)

app.listen(port, () => {
  console.log(`Serveur en écoute`)
})

module.exports = app;